#+TAGS: { @musings(M) @code(C) @blog(B) }
#+TAGS: [ infra(i) devops(d) powershell(p) terraform(t) kubernetes(k) emacs(e) coding(c) ]
#+HUGO_BASE_DIR: ./
#+HUGO_AUTO_SET_LASTMOD: t
#+MACRO: more <!--more-->

* TopLevel
  :PROPERTIES:
:EXPORT_HUGO_SECTION: ./
:EXPORT_HUGO_CUSTOM_FRONT_MATTER: :author false :toc false
:END:
** About
:PROPERTIES:
:EXPORT_FILE_NAME: about
:EXPORT_HUGO_PUBLISHDATE: 2018-08-16
:EXPORT_HUGO_CUSTOM_FRONT_MATTER: :nocomments t
:END:

*** Author

My name is Jonathan Leech-Pepin although I typically go by Jon.
Online I typically use ~jleechpe~ or something similar.  I'm currently
employed as a DevOps engineer managing infrastructure as code
primarily using Terraform, Puppet and Kubernetes.

I've spent the last 10 years in the IT field starting as a Help Desk
intern and progressing from working as On-Site support to being a
System/Domain admin and most recently working in DevOps.  My
background is primarily in user and systems support with a very strong
emphasis on PowerShell for automation and tooling.

At home I run a mixture of Windows, primarily for gaming, and
ArchLinux machines.

*** Blog

This blog is generated from a single org-mode file using ~ox-hugo~ and
hosted on [[https://www.netlify.com][Netlify]].

** Projects
:PROPERTIES:
:EXPORT_FILE_NAME: projects
:EXPORT_HUGO_PUBLISHDATE: 2018-08-16
:EXPORT_HUGO_CUSTOM_FRONT_MATTER: :nocomments t
:END:
In progress...

* Posts
:PROPERTIES:
:EXPORT_HUGO_SECTION: posts
:EXPORT_HUGO_CUSTOM_FRONT_MATTER: :toc false
:END:

** Musings                                                         :@musings:

*** DONE DevOps from the Infra side                            :infra:devops:
:PROPERTIES:
:EXPORT_FILE_NAME: devops_from_infra
:END:

I spent last weekend at [[https://www.codeonthebeach.com][Code on the beach 2018]].  I got to talk to a
lot of interesting people and hear points of view I wouldn't normally
get a chance to hear.  As a DevOps engineer with an infrastructure
background I primarily focused on talks about coding and testing
practices.  Plenty to takeaway from those, but they didn't spark the
same deep thoughts as the DevOps focused talks I went to.

{{{more}}}

A large focus of those talks was on how using a provisioner such as
Terraform or a cloud container service like Kubernetes would simplify
life and remove the need for Ops to ensure the hardware was up and
running when the developer needed it.  I'm in no shape or form going
to dispute those facts, Kubernetes in particular gives developers very
powerful method to create the applications they want without really
worrying about what is going on behind the scenes.  As long as the
cluster has resources, their code should be able to behave as
intended.

What the talks did not emphasize in the same way was the benefits it
gives to Operations.  I don't have to spend hours waiting for burn-in,
or worry about which checkbox was checked setting up the software on
the machine.  I can create templates, create functions and tooling
that lets me build the code to provision the hardware for a developer.
Terraform has it's faults, although 0.12 should remedy at least some
of those, but it provides declarative provisioning for virtual
machines.  You can even use it to manage Kubernetes deployments.  I
wouldn't, but you could.

These tools empower, and teach, Ops Engineers to look at
infrastructure management from a developer mindset.  The same benefits
devs get, not having to worry about the physical hardware, are there
for Ops.  A project comes in wanting a web server, a database and all
the various firewall rules to allow communication and instead of
figuring out what hardware needs to be bought I open up my editor of
choice and write out the appropriate provisioning (Terraform) and
configuration (Puppet) code, commit it to version control and deploy
it.  The code is validated, in an ideal world tested, and then the
build is submitted and completed.  Same process, same result, just
instead of a shiny new API or frontend, I provide the credentials and
IP address and let the Dev or DBA implement their solution.  If I get
enough of these requests, I'll write modular code and have even less
work to do for the next because all that I'll need to include is the
parameters, the rest is handled by the logic I've previously defined.

And since it's all code, I get access to all kinds of Dev tools and
methodologies that traditional infrastructure lacked.  I can test my
code, both before deployment and once then the infrastructure once
deployed to ensure what was created matched expectations.  I can use
scaffold my code, create templates and code generators to reduce Copy
Paste errors or duplicated work.  I can track my changes and revert
back to a previous version if something breaks.  For a Dev this might
not sound like much, but for infrastructure it changes everything.

One of the intended takeaways from the DevOps talks was the reduced
reliance on Ops to provision and configure servers, speeding up the
development cycle.  My takeaway is that I can spin up more servers,
more infrastructure, with more consistency in less time.

Pay no attention to the Ops Engineer behind the curtain.  The
infrastructure will be there when you need it, it might not be shiny
or easy to show off the way a front end or API is, but without the
infrastructure, there's nowhere for those to run.  Even a Kubernetes
cluster needs someone to configure it and ensure developers aren't
overwriting each other's changes.  I'll go back to waiting for my
infrastructure to compile, and figuring out how the JavaScript and C#
code being deployed on it works...

*** DONE Self-Rating your skills with a technology        :coding:powershell:
:PROPERTIES:
:EXPORT_FILE_NAME: self_rated_skills
:END:

More than once in my career I've had a recruiter or interviewer ask me
to rate my skill with an aspect of technology.  Often it's been a case
of "/On a scale of 1 to 10 how would you rate your skill with <x>/".
Although I can understand why some people would want this sort of
self-assessment, it omits something very important, the frame of
reference being used.

{{{more}}}

What is a 1, and much more importantly, what is a 10.  I'm going to
use PowerShell for my frame of reference due to familiarity, but any
language or skill can apply.

Split the scale into Beginner (1-3), Intermediate (4-6) and
Advanced/Expert (7-9) and you'll likely get consensus.  I'm excluding
10 as an answer because if you're good enough that a 10 is the right
answer then you probably shouldn't need to be asked how skilled you
are.

Beginner is fairly easy.  You know what Powershell is, how to open it,
how to run the basic commands that parallel Dos, Bash, or any other
shell.

Intermediate... you know how to write scripts, how to use parameters,
how to create a function... No?  Functions would be closer to a 7 in
your mind?  Then advanced would be creating advanced functions,
creating a Module.

Take another person and they consider Advanced functions somewhere
around a 5, maybe a 6.  Modules become a 6 or 7.  Dynamic parameters
are quite advanced.  But that still leaves creating cohesive modules
that leverage other modules.  Splitting work into individual functions
to leverage re-usability, writing wrapper functions as a DSL.  And
Classes in PowerShell 5 and upwards, along with runspaces, jobs,
optimization based on pipeline and object stacks, and sundry other
elements.

Of course the job might not require any of the Advanced/Expert skills,
or only a subset.  It might need you to be familiar with writing
tests, which is a skill category in and of itself.  But then the
self-assessment question should specify the context against which to
compare.

Asking for this sort of Self-Assessment is, in my mind, only
moderately more useful than having someone complete a
skills-assessment quiz where a large portion of the questions can be
answered by using a Search Engine unless you happen to have memorized
implementation or configuration details.  Give someone a problem to
solve, a scenario to work out.  Discuss how they would approach the
situation, or solve the problem.  I might not remember exactly which
variables let me configure Dynamic Parameters in Powershell, but I can
tell you when and why I might use it, or how I can leverage it to
improve code behavior.

If you must ask for someone's skill level, give them examples to
compare themselves to, or better yet, let them rate themselves, but
then ask them where they would rate one or more examples of skills
and/or knowledge about the subject.  You get to see how confident they
are in what they know while at the same time gaining an idea of what
that rating means to them.

*** DONE Coding and music                                            :coding:
:PROPERTIES:
:EXPORT_FILE_NAME: coding_and_music
:END:

This initially came up briefly at a CotB2018 after party but I'd
forgotten about it until late last week when a conversation with
coworkers led to a very similar conclusion.

We were talking about rating one's skill level with various languages
or tools and how your rating depends very strongly on what you know
about the language, more on the topic at a future date.  While trying
to formulate my thoughts on some of the challenges of learning to code
I stumbled upon an analogy that seemed to fit perfectly.  Learning to
code is like learning to play and create music...

{{{more}}}

My coding and developer skills are primarily self-taught.  I took
CompSci 101 (Java) in University and some basic C++ in High School.
Everything else comes from blogs, YouTube and a few Pluralsight
courses (typically Intermediate and Advanced topics regarding specific
concepts or methodologies once I knew the basics).  I won't try to
rate my abilities overall, suffice to say my main strength at this
time is PowerShell and I'm familiar enough with a handful of languages
to be dangerous, or to read someone's code and get an idea for what
it's meant to do.

As to the conversation and how it led to music, we were talking about
learning to program and how there's a wall to be overcome early on in
programming courses.  I suspect this wall is more common in academic
learning than in self-taught simply because it's the realization of "I
can use it for that?" or "It lets me do this?".  Approaching it on my
own I had goals and ideas of what to do, I didn't look at it as an
easy course, or something I /should/ take because it was recommended.
I learned to program to make things happen, be it automate my job, get
more information from a browser game or configure my editor (Emacs).

People tend to get past that wall fairly quickly, or get stopped by
it from what I've seen.  You can grasp the point of coding, or at
least the point of copying existing code and scripts, or it's an
arcane tool.  This is similar to learning music.  You can learn to
read sheet music, guitar tab, or even play by ear.  You can learn what
notes are which and how they progress, or instruments are those things
you like listening to but best not touch.

The wall I was trying to describe isn't quite so clear cut.  It
doesn't prevent you from making a career of programming, it isn't the
point between adapting existing code and writing 'new' code for
purposes you've never approached.  Progress along those lines is a
matter of time and experience more than anything else.  The second
wall is the one where the music analogy is more useful.

Lots of people can play music, computers can play music, or at least
beep music out.  But simply playing the notes as written doesn't
convey music fully.  Music conveys feeling, emotion, mood.  Certain
musicians can convey this better than others while playing the exact
same notes.  It isn't just the sounds, it's the art behind the sounds.

The same applies to coding.  Some code has an elegance to it.  There's
multiple ways to solve the same problem, equally valid and in many
cases equally effective.  The second wall is a creative one, an
abstract wall between functional code and elegant code.  I've seen
both types of code, there is a time and a place for each.  Even when
the only thing that's needed is purely functional code, there's still
something to be said for artistry.

Code, music, they're both patterns, and they both can sometimes give a
glimpse into the mind, or passion, of their creator.

---

In the end you could compare code to any artistic endeavor.  Music
simply allows for a plethora of examples most people can draw on.

** Blog                                                               :@blog:
*** DONE Setting up a blog with ox-hugo and netlify      :hugo:netlify:emacs:
:PROPERTIES:
:EXPORT_FILE_NAME: blog_setup
:END:

I've looked at several static blogging options over the last few years
although I've never actually set up a blog beyond anything
Proof-of-Concept like that had no real content to it.  For a long time
I had planned to self-host on a small VPS and ensure the content was
to my liking rather than rely on some form of hosting that would
include ads or other content I didn't intend for.

Along the way I'd decided I wanted to use a static blog engine.  As
long as the theme was what I desired, or something close enough to
convert, it simplified deployment and upkeep.  It also meant I'd most
likely be able to use some form of marked up text, rather than using a
rich text editor that was browser-only or needing complex tags to
ensure the post came out as I wanted.

I'd initially been leaning towards using Jekyll with Github pages, but
since I prefer Org-Mode to Markdown I went looking to see if there was
a converter and ran across a thread on the [[http://lists.gnu.org/archive/html/emacs-orgmode/2018-04/msg00582.html][Org-Mode mailing list]]
specifically dealing with moving to (or using) Org instead of Jekyll.
I'd seen mention of ox-hugo before but never really looked into hugo
beyond noting it as an alternative to Jekyll and a possible blogging
tool on [[https://orgmode.org/worg/org-blog-wiki.html][Worg]].

In the end the setup is quite simple:
1. A single org-mode file containing the actual content of the blog for
   export.
2. A gitlab repo containing the file and the basic content generated
   with =hugo new site=, my theme of choice, Nix, and the appropriate
   =config.toml= settings to provide configuration
   - The theme is slightly modified to fit my desires.  The RSS icon
     is not default and the copyright would normally be the blog title
     rather than the username.
3. A [[https://www.netlify.com][Netlify]] account set up with a build-on-commit hook pointed at the
   master branch to ensure immediate publication.

** Code                                                               :@code:

*** TODO Testing infrastructure with Inspec                    :infra:devops:
:PROPERTIES:
:EXPORT_FILE_NAME: testing_infra_with_inspec
:END:

*** TODO Converting existing Azure Infrastructure to Terraform :infra:devops:terraform:
:PROPERTIES:
:EXPORT_FILE_NAME: tf_existing_azure
:END:
