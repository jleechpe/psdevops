+++
title = "About"
publishDate = 2018-08-16
lastmod = 2018-08-21T18:30:18-04:00
draft = false
nocomments = "t"
+++

## Author {#author}

My name is Jonathan Leech-Pepin although I typically go by Jon.
Online I typically use `jleechpe` or something similar.  I'm currently
employed as a DevOps engineer managing infrastructure as code
primarily using Terraform, Puppet and Kubernetes.

I've spent the last 10 years in the IT field starting as a Help Desk
intern and progressing from working as On-Site support to being a
System/Domain admin and most recently working in DevOps.  My
background is primarily in user and systems support with a very strong
emphasis on PowerShell for automation and tooling.

At home I run a mixture of Windows, primarily for gaming, and
ArchLinux machines.


## Blog {#blog}

This blog is generated from a single org-mode file using `ox-hugo` and
hosted on [Netlify](https://www.netlify.com).
