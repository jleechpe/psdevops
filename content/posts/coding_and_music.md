+++
title = "Coding and music"
lastmod = 2018-09-11T22:56:55-04:00
tags = ["coding"]
categories = ["musings"]
draft = false
toc = false
+++

This initially came up briefly at a CotB2018 after party but I'd
forgotten about it until late last week when a conversation with
coworkers led to a very similar conclusion.

We were talking about rating one's skill level with various languages
or tools and how your rating depends very strongly on what you know
about the language, more on the topic at a future date.  While trying
to formulate my thoughts on some of the challenges of learning to code
I stumbled upon an analogy that seemed to fit perfectly.  Learning to
code is like learning to play and create music...

<!--more-->

My coding and developer skills are primarily self-taught.  I took
CompSci 101 (Java) in University and some basic C++ in High School.
Everything else comes from blogs, YouTube and a few Pluralsight
courses (typically Intermediate and Advanced topics regarding specific
concepts or methodologies once I knew the basics).  I won't try to
rate my abilities overall, suffice to say my main strength at this
time is PowerShell and I'm familiar enough with a handful of languages
to be dangerous, or to read someone's code and get an idea for what
it's meant to do.

As to the conversation and how it led to music, we were talking about
learning to program and how there's a wall to be overcome early on in
programming courses.  I suspect this wall is more common in academic
learning than in self-taught simply because it's the realization of "I
can use it for that?" or "It lets me do this?".  Approaching it on my
own I had goals and ideas of what to do, I didn't look at it as an
easy course, or something I _should_ take because it was recommended.
I learned to program to make things happen, be it automate my job, get
more information from a browser game or configure my editor (Emacs).

People tend to get past that wall fairly quickly, or get stopped by
it from what I've seen.  You can grasp the point of coding, or at
least the point of copying existing code and scripts, or it's an
arcane tool.  This is similar to learning music.  You can learn to
read sheet music, guitar tab, or even play by ear.  You can learn what
notes are which and how they progress, or instruments are those things
you like listening to but best not touch.

The wall I was trying to describe isn't quite so clear cut.  It
doesn't prevent you from making a career of programming, it isn't the
point between adapting existing code and writing 'new' code for
purposes you've never approached.  Progress along those lines is a
matter of time and experience more than anything else.  The second
wall is the one where the music analogy is more useful.

Lots of people can play music, computers can play music, or at least
beep music out.  But simply playing the notes as written doesn't
convey music fully.  Music conveys feeling, emotion, mood.  Certain
musicians can convey this better than others while playing the exact
same notes.  It isn't just the sounds, it's the art behind the sounds.

The same applies to coding.  Some code has an elegance to it.  There's
multiple ways to solve the same problem, equally valid and in many
cases equally effective.  The second wall is a creative one, an
abstract wall between functional code and elegant code.  I've seen
both types of code, there is a time and a place for each.  Even when
the only thing that's needed is purely functional code, there's still
something to be said for artistry.

Code, music, they're both patterns, and they both can sometimes give a
glimpse into the mind, or passion, of their creator.

---

In the end you could compare code to any artistic endeavor.  Music
simply allows for a plethora of examples most people can draw on.
