+++
title = "Setting up a blog with ox-hugo and netlify"
lastmod = 2018-08-22T21:13:08-04:00
tags = ["hugo", "netlify", "emacs"]
categories = ["blog"]
draft = false
toc = false
+++

I've looked at several static blogging options over the last few years
although I've never actually set up a blog beyond anything
Proof-of-Concept like that had no real content to it.  For a long time
I had planned to self-host on a small VPS and ensure the content was
to my liking rather than rely on some form of hosting that would
include ads or other content I didn't intend for.

Along the way I'd decided I wanted to use a static blog engine.  As
long as the theme was what I desired, or something close enough to
convert, it simplified deployment and upkeep.  It also meant I'd most
likely be able to use some form of marked up text, rather than using a
rich text editor that was browser-only or needing complex tags to
ensure the post came out as I wanted.

I'd initially been leaning towards using Jekyll with Github pages, but
since I prefer Org-Mode to Markdown I went looking to see if there was
a converter and ran across a thread on the [Org-Mode mailing list](http://lists.gnu.org/archive/html/emacs-orgmode/2018-04/msg00582.html)
specifically dealing with moving to (or using) Org instead of Jekyll.
I'd seen mention of ox-hugo before but never really looked into hugo
beyond noting it as an alternative to Jekyll and a possible blogging
tool on [Worg](https://orgmode.org/worg/org-blog-wiki.html).

In the end the setup is quite simple:

1.  A single org-mode file containing the actual content of the blog for
    export.
2.  A gitlab repo containing the file and the basic content generated
    with `hugo new site`, my theme of choice, Nix, and the appropriate
    `config.toml` settings to provide configuration
    -   The theme is slightly modified to fit my desires.  The RSS icon
        is not default and the copyright would normally be the blog title
        rather than the username.
3.  A [Netlify](https://www.netlify.com) account set up with a build-on-commit hook pointed at the
    master branch to ensure immediate publication.
