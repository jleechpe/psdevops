+++
title = "DevOps from the Infra side"
lastmod = 2018-08-20T17:56:27-04:00
tags = ["infra", "devops"]
categories = ["musings"]
draft = false
toc = false
+++

I spent last weekend at [Code on the beach 2018](https://www.codeonthebeach.com).  I got to talk to a
lot of interesting people and hear points of view I wouldn't normally
get a chance to hear.  As a DevOps engineer with an infrastructure
background I primarily focused on talks about coding and testing
practices.  Plenty to takeaway from those, but they didn't spark the
same deep thoughts as the DevOps focused talks I went to.

<!--more-->

A large focus of those talks was on how using a provisioner such as
Terraform or a cloud container service like Kubernetes would simplify
life and remove the need for Ops to ensure the hardware was up and
running when the developer needed it.  I'm in no shape or form going
to dispute those facts, Kubernetes in particular gives developers very
powerful method to create the applications they want without really
worrying about what is going on behind the scenes.  As long as the
cluster has resources, their code should be able to behave as
intended.

What the talks did not emphasize in the same way was the benefits it
gives to Operations.  I don't have to spend hours waiting for burn-in,
or worry about which checkbox was checked setting up the software on
the machine.  I can create templates, create functions and tooling
that lets me build the code to provision the hardware for a developer.
Terraform has it's faults, although 0.12 should remedy at least some
of those, but it provides declarative provisioning for virtual
machines.  You can even use it to manage Kubernetes deployments.  I
wouldn't, but you could.

These tools empower, and teach, Ops Engineers to look at
infrastructure management from a developer mindset.  The same benefits
devs get, not having to worry about the physical hardware, are there
for Ops.  A project comes in wanting a web server, a database and all
the various firewall rules to allow communication and instead of
figuring out what hardware needs to be bought I open up my editor of
choice and write out the appropriate provisioning (Terraform) and
configuration (Puppet) code, commit it to version control and deploy
it.  The code is validated, in an ideal world tested, and then the
build is submitted and completed.  Same process, same result, just
instead of a shiny new API or frontend, I provide the credentials and
IP address and let the Dev or DBA implement their solution.  If I get
enough of these requests, I'll write modular code and have even less
work to do for the next because all that I'll need to include is the
parameters, the rest is handled by the logic I've previously defined.

And since it's all code, I get access to all kinds of Dev tools and
methodologies that traditional infrastructure lacked.  I can test my
code, both before deployment and once then the infrastructure once
deployed to ensure what was created matched expectations.  I can use
scaffold my code, create templates and code generators to reduce Copy
Paste errors or duplicated work.  I can track my changes and revert
back to a previous version if something breaks.  For a Dev this might
not sound like much, but for infrastructure it changes everything.

One of the intended takeaways from the DevOps talks was the reduced
reliance on Ops to provision and configure servers, speeding up the
development cycle.  My takeaway is that I can spin up more servers,
more infrastructure, with more consistency in less time.

Pay no attention to the Ops Engineer behind the curtain.  The
infrastructure will be there when you need it, it might not be shiny
or easy to show off the way a front end or API is, but without the
infrastructure, there's nowhere for those to run.  Even a Kubernetes
cluster needs someone to configure it and ensure developers aren't
overwriting each other's changes.  I'll go back to waiting for my
infrastructure to compile, and figuring out how the JavaScript and C#
code being deployed on it works...
