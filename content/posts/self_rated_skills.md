+++
title = "Self-Rating your skills with a technology"
lastmod = 2018-10-01T21:33:46-04:00
tags = ["coding", "powershell"]
categories = ["musings"]
draft = false
toc = false
+++

More than once in my career I've had a recruiter or interviewer ask me
to rate my skill with an aspect of technology.  Often it's been a case
of "_On a scale of 1 to 10 how would you rate your skill with <x>_".
Although I can understand why some people would want this sort of
self-assessment, it omits something very important, the frame of
reference being used.

<!--more-->

What is a 1, and much more importantly, what is a 10.  I'm going to
use PowerShell for my frame of reference due to familiarity, but any
language or skill can apply.

Split the scale into Beginner (1-3), Intermediate (4-6) and
Advanced/Expert (7-9) and you'll likely get consensus.  I'm excluding
10 as an answer because if you're good enough that a 10 is the right
answer then you probably shouldn't need to be asked how skilled you
are.

Beginner is fairly easy.  You know what Powershell is, how to open it,
how to run the basic commands that parallel Dos, Bash, or any other
shell.

Intermediate... you know how to write scripts, how to use parameters,
how to create a function... No?  Functions would be closer to a 7 in
your mind?  Then advanced would be creating advanced functions,
creating a Module.

Take another person and they consider Advanced functions somewhere
around a 5, maybe a 6.  Modules become a 6 or 7.  Dynamic parameters
are quite advanced.  But that still leaves creating cohesive modules
that leverage other modules.  Splitting work into individual functions
to leverage re-usability, writing wrapper functions as a DSL.  And
Classes in PowerShell 5 and upwards, along with runspaces, jobs,
optimization based on pipeline and object stacks, and sundry other
elements.

Of course the job might not require any of the Advanced/Expert skills,
or only a subset.  It might need you to be familiar with writing
tests, which is a skill category in and of itself.  But then the
self-assessment question should specify the context against which to
compare.

Asking for this sort of Self-Assessment is, in my mind, only
moderately more useful than having someone complete a
skills-assessment quiz where a large portion of the questions can be
answered by using a Search Engine unless you happen to have memorized
implementation or configuration details.  Give someone a problem to
solve, a scenario to work out.  Discuss how they would approach the
situation, or solve the problem.  I might not remember exactly which
variables let me configure Dynamic Parameters in Powershell, but I can
tell you when and why I might use it, or how I can leverage it to
improve code behavior.

If you must ask for someone's skill level, give them examples to
compare themselves to, or better yet, let them rate themselves, but
then ask them where they would rate one or more examples of skills
and/or knowledge about the subject.  You get to see how confident they
are in what they know while at the same time gaining an idea of what
that rating means to them.
